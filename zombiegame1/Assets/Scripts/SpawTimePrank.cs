﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawTimePrank : MonoBehaviour
{
    public Transform SpawPrank;
    public GameObject Dragon;
    private float rand;

    public Transform[] SpawMouse;
    public GameObject Mouse;
    private float RandMouse;

    public Transform SpawKong;
    public GameObject Kong;
    public GameObject Kong1;
    public Transform SpawKong1;
    private float RandKong;

    
    // Start is called before the first frame update
    void Start()
    {        
        rand = Random.Range(100f,110f);
        RandMouse = Random.Range(110f, 120f);
        RandKong = Random.Range(125f, 130f);
        StartCoroutine(SpawPrankDragon());
        StartCoroutine(SpMouse());
        StartCoroutine(SpKong());
    }
        
    IEnumerator SpawPrankDragon()
    {
        yield return new WaitForSeconds(rand);        
        Instantiate(Dragon, SpawPrank.position, Quaternion.identity);
        Start();
    }

    IEnumerator SpMouse()
    {
        yield return new WaitForSeconds(RandMouse);
        int RandSpaw = Random.Range(0, 2);
        Instantiate(Mouse, SpawMouse[RandSpaw].position, Quaternion.identity);
        Start();
    }
    IEnumerator SpKong()
    {
        yield return new WaitForSeconds(RandKong);
        Instantiate(Kong, SpawKong.position, Quaternion.identity);
        yield return new WaitForSeconds(3f);
        StartCoroutine(SpKong1());
    }

    IEnumerator SpKong1()
    {
        Instantiate(Kong1, SpawKong1.position, Quaternion.identity);
        yield return new WaitForSeconds(1f);
        Start();
    }
}

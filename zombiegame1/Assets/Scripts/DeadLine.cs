﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadLine : MonoBehaviour
{
    public Player _player;


    void OnTriggerEnter2D(Collider2D col)
    {
        StartCoroutine(_player.GameOver());
    }

    
}

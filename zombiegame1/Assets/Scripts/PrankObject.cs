﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrankObject : MonoBehaviour
{   
    public float speed = 1f;
    public float time = 20;
    public Vector2 position;
       

    private void Update()
    {
        transform.Translate(speed * position * Time.deltaTime, Space.World);
        StartCoroutine(PrankStart());
    }
    // Update is called once per frame

    IEnumerator PrankStart()
        {
            //Debug.Log("start BulletTime");
            yield return new WaitForSeconds(time);
            Destroy(this.gameObject);
            
        }
    
}

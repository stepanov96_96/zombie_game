﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject Bullet; //Снаряд
    public GameObject BulletFly;//2 Снаряд
    public Transform firePoint; //Точка, с которой будут отправляться снаряды и лучи
    public List<GameObject> Box = new List<GameObject>();
    public GameControl gameControl;    
    public float speed = 20f;
    private Rigidbody2D rb;    
    public AudioSource ShootSound;
    public AudioSource DieGranyCats;
    public int PlayerHealth = 100;    
    //public Animator GranyDead;
    public static Animator anim;
   // public Animator ZomCats;
    public static bool IsShootingNow = false;
    public bool going = true;
    public float yMin;
    public float yMax;
    public static int DamageForEnemy = 1;


    public static bool moveUp;
    public static bool moveDown;

    //public static bool ShotBullet;
    public static bool UpCane;
    //public static bool a;

    public GameObject GameOverUI;
    public GameObject GameOverText;
    public GameObject CanvasMenu;
    public GameObject CanvasGame;

    public static bool GameIsPaused = false;
    //public Animation Game_Over_Text;

    int heightScreen = 0;
    void Start()
    {
        
        float moveY = Input.GetAxis("Vertical");
        anim = GetComponent<Animator>();        
        heightScreen = Screen.height;
        GameIsPaused = false;
        IsShootingNow = false;
        GameControl.ShotBullet = false;
        GameControl.UpShotBullet = false;
        DamageForEnemy = 1;
        moveUp=false;
        moveDown=false;
}

    public void FixedUpdate()
    {
        #region Komennt
        //стрельба
        //if (Input.GetButtonDown("Fire2") && !IsShootingNow)  //Если игрок нажал на e
        //{

        //    //Вызов метода стрельбы снарядами            
        //    ShootBulletFly();
        //    ShootSound.Play();
        //}

        //if (ShotBullet && !IsShootingNow)
        //{
        //    ShootBulletFly();
        //    ShootSound.Play();
        //}



        //if (Input.GetButtonDown("Fire1") && !IsShootingNow)  //Если игрок нажал на q
        //{

        //    //Вызов метода стрельбы снарядами
        //    StartCoroutine(GranyAttack());
        //    ShootBullet();
        //    ShootSound.Play();
        //    /*/if (Input.GetButtonDown("Fire1") && !IsShootingNow)  //Если игрок повторно нажал на q
        //    {
        //        anim.SetBool("isGrunyAttack", false);
        //        StartCoroutine(GranyAttack());
        //        ShootBullet();
        //        ShootSound.Play();

        //    }/*/



        //    //Debug.Log("GranyAttack");
        //}


        ////ходьба

        //float yP = Camera.main.WorldToScreenPoint(transform.position).y;
        //rb = GetComponent<Rigidbody2D>();
        //float moveY = Input.GetAxis("Vertical");
        //bool isMoveUp = true;


        ////игра с клавиатуры
        //if (Input.GetKey(KeyCode.UpArrow) && yP <= 560)
        //{
        //    rb.MovePosition(rb.position + Vector2.up * moveY * speed * Time.deltaTime);
        //    anim.SetBool("isGrunyRun", true);


        //}
        //else
        //{
        //    anim.SetBool("isGrunyRun", false);

        //}

        //if (Input.GetKey(KeyCode.DownArrow) && yP >= 189)
        //{
        //    rb.MovePosition(rb.position + Vector2.up * moveY * speed * Time.deltaTime);
        //    anim.SetBool("isGrunyRun", true);

        //}


        ///*/привязка кнопок в передвижении                     
        //if (moveUp && yP <= 560)
        //{
        //    rb.MovePosition(rb.position + Vector2.up * moveY * speed * Time.deltaTime);
        //    anim.SetBool("isGrunyRun", true);
        //    //Debug.Log("MoveUpPlayer");
        //}
        //else
        //{
        //    anim.SetBool("isGrunyRun", false);

        //}

        //if (moveDown && yP >= 189)
        //{
        //    rb.MovePosition(rb.position + Vector2.up * moveY * speed * Time.deltaTime);
        //    anim.SetBool("isGrunyRun", true);
        //    //Debug.Log("MoveDownPlayer1");

        //}/*/
        #endregion Komennt
    }

    private void Update()
    {
        float yP = Camera.main.WorldToScreenPoint(transform.position).y;
        rb = GetComponent<Rigidbody2D>();
        float moveY = Input.GetAxis("Vertical");
        bool isMoveUp = true;
        float yMoveStep = 0.05f;

        if (moveUp && yP <= ((float)Screen.height / 2.1f))
        {            
            transform.position = new Vector3(transform.position.x, transform.position.y + yMoveStep, transform.position.z);            
            anim.SetBool("isGrunyRun", true);            
        }
        else
        {
            //Debug.Log("Got Up1");
            anim.SetBool("isGrunyRun", false);

        }

        if (moveDown && yP >= ((float)Screen.height / 5f))
        {               
            transform.position = new Vector3(transform.position.x, transform.position.y - yMoveStep, transform.position.z);
            anim.SetBool("isGrunyRun", true);         

        }

    }


    #region Shoot

    public void ShootBullet()
    {
        
        GameObject newBull = Instantiate(Bullet, firePoint.position, firePoint.rotation);
        newBull.GetComponent<Bullet>().gameControl = gameControl;
        GameControl.isShooting = true;
        IsShootingNow = true;
        StartCoroutine(GranyAttack());        

    }

   public void ShootBulletFly()
    {        
        GameObject newBulletFly = Instantiate(BulletFly, firePoint.position, firePoint.rotation);
        newBulletFly.GetComponent<BulletFly>().gameControl = gameControl;
        GameControl.isShooting = true;
        IsShootingNow = true;
        if (LoadMenu.Sound == true)
        {
            ShootSound.Play();
        }
        
    }

    #endregion Shoot

    void Dead()
    {

        if (PlayerHealth <= 0)
        {
            //PlayerDead.SetTrigger("isDead");
        }
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Contains("ZomCat"))
        {
            GameControl.isStopGame = true;
            
            anim.SetTrigger("dead");
            StartCoroutine(GameOver());
            if (LoadMenu.Sound==true)
            {
                DieGranyCats.Play();
            }            
        }
                
                
    }



    public IEnumerator GranyAttack()
    {
        
        anim.SetBool("isGrunyAttack", true);
        yield return new WaitForSeconds(0.01f); //задержка         
        anim.SetBool("isGrunyAttack", false);
        

    }

    public IEnumerator GameOver()
    {
        CanvasMenu.SetActive(false);
        CanvasGame.SetActive(false);
        GameOverText.SetActive(true);
        GameIsPaused = true;
        yield return new WaitForSeconds(2f);
        GameOverText.SetActive(false);        
        GameOverUI.SetActive(true);
        Time.timeScale = 0f;
        
        
    }
        

}

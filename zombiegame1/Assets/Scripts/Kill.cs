﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kill : MonoBehaviour
{
    public GameControl gameControl;
    public static int healthCats = 1;
    public static int NewHealthCats = 1;
    public Animator ZomDead;
    public Transform SpawDust;//точка воспроизведения дыма
    public GameObject Dust;
    public AudioSource DieZomCats;




    void Start()
    {
        ZomDead = GetComponent<Animator>();        
    }


    public void TakeDamage()
    {
        //Debug.Log("upDamage0-"+Player.DamageForEnemy);
        //Debug.Log("healthCats1-"+healthCats);
        healthCats -= Player.DamageForEnemy;
        if (healthCats <= 0)
        {
            //Debug.Log("helth");
            StartCoroutine(Zom());
            healthCats = NewHealthCats;
            //NewHealthCats++;
            //Debug.Log("DeadCats2-"+healthCats);
            
        }
        
    }
        
    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag.Contains("Player"))
        {
            
            ZomDead.SetBool("idle",true);
        }
        

    }
    
    //удаление котов
    IEnumerator Zom()
    {
        ZomDead.SetTrigger("ZomDead");
        Instantiate(Dust, SpawDust.position, Quaternion.identity);             
        //GameControl.score1();
        gameControl.DestroyZomCat(this.gameObject);              
        yield return new WaitForSeconds(1f); //задержка     
        Destroy(gameObject);
        if (LoadMenu.Sound == true)
        {
            DieZomCats.Play();
        }
        

    }

   
}

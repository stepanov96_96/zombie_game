﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerControl : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    private Joystick joyctick;

    public GameControl gameControl;
    public Player _player;
    private ButtonControl player;
    public EnumButton enumButton;


    void Start()
    {
        joyctick = gameObject.GetComponent<Joystick>(); 
    }

    

    //void Update()
    //{
    //    if (joyctick.Vertical > 0.1f)
    //    {
    //        ButtonControl.UpArrow();
            
    //    }
    //    else if (joyctick.Vertical < -0.1f)
    //    {
    //       ButtonControl.DownArrow();

    //    }
        
    //}
    
    public void OnPointerDown(PointerEventData eventData)
    {
        switch (enumButton)
        {
            case EnumButton.ArrowUp:
                Player.moveUp = true;            
                break;
            case EnumButton.ArrowDown:
                Player.moveDown = true;
                                break;
            case EnumButton.Shot:
                GameControl.ShotBullet = true;
                Debug.Log("shottrue");
                break;
            default:
                break;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (enumButton)
        {
            case EnumButton.ArrowUp:
                Player.moveUp = false;                                        
                break;
            case EnumButton.ArrowDown:
                Player.moveDown = false;
                //Debug.Log("falseDown");
                break;
            case EnumButton.Shot:
                GameControl.ShotBullet = false;
                Debug.Log("shotfalse");
                break;
            default:
                break;
        }
     }
}

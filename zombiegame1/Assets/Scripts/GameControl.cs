﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameControl : MonoBehaviour
{
    #region Variables

    public Kill gameControl;
    public List<GameObject> ZomCatList = new List<GameObject>();    
    private Rigidbody2D rb;
    public float speed = 10;    
    public GameObject[] ZomCat;    
    public Transform[] spawnPoints;
    float LastTimeSpaw = 0f;
    float frequncySpaw = 1f;
    bool _IsSpawNow = true;    
    int CountadvNow = 0;
    int CurrentLevel = 0;
   // public int level = 1;
    public int Nextlevel = 3;
    private Animator ZomDie;
    public static bool isShooting = false;
    public static bool  isStopGame = false;
    public static bool isSave = false;
    public static int MaxCatSpaw = 1;
    public static int CatSpawedFromStart = 0;
    public static bool ShotBullet;
    public static bool UpShotBullet;
    public Animator ZomIdle;
    public int hil;
    int SpawCat = 10;
    public float SpedCats = 1f;
    

    //счетчик времени задержки
    public int TimeSkore=5;
    public Text TimeText;
    
    //Счетчик убийств
    private int Score;
    public Text ScoreText;
    //public Text HighScore;
    public static int HighScoreCounter;

    //Счетчик уровней
    public int Level=1;
    public Text LevelPlus;
    

    //Счетчик волн
    public int WavesSkore = 1;
    public Text WavesText;

    //счетчик монет
    public int CoinCounter = 0;
    public Text CoinText;

    public Player _player;

    public float repeat_time; /* Время в секундах */
    private float curr_time=1;

    public static GameControl instance;
    #endregion Variables

    private void Awake()
    {
        instance = this;
        if (PlayerPrefs.HasKey("SaveScore"))
        {
            HighScoreCounter = PlayerPrefs.GetInt("SaveScore");
            //HighScore.text = HighScoreCounter + "";
        }        
    }

    void Start()
    {
       
        isShooting = false;
        MaxCatSpaw = 1;
        CatSpawedFromStart = 0;
        Kill.healthCats = 1;
        Kill.NewHealthCats = 1;
        Player.DamageForEnemy = 1;
        Player.GameIsPaused = false;
        isStopGame = false;
        ZomIdle = GetComponent<Animator>();
        GameIsPaused = false;
        Time.timeScale = 1f;
        TimeSkore = 5;

        if (isSave == false)
        {
            Score = 0;
            ScoreText.text = Score + "";

            Level = 1;
            LevelPlus.text = Level + "";

            WavesSkore = 1;
            WavesText.text = WavesSkore + "";
        }

        if (isSave == true)
        {            
                WavesSkore = PlayerPrefs.GetInt("SaveWavesSkore");//запись волны            
            if (WavesSkore==0)
            {
                WavesSkore = 1;
            }
            WavesText.text = WavesSkore + "";            
            MaxCatSpaw = PlayerPrefs.GetInt("SaveMaxSpawCats");//запись количество котов    
            Level = PlayerPrefs.GetInt("SaveLevel");//Запись Уровня            
             if (Level == 0)
            {
                Level = 1;
            }
            LevelPlus.text = Level + "";
                Score = PlayerPrefs.GetInt("SaveScoreLevel");//Запись счета            
            ScoreText.text = Score + "";                       
                Kill.NewHealthCats = PlayerPrefs.GetInt("SaveHealthCats");//Запись жизни кота         
            isSave = false;
        }
        

    }

    void Spawn()
    {
        
        float rand = Random.Range(0.5f, SpedCats);
        int RandCat = Random.Range(0, 3);
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        GameObject newZomCat = Instantiate(ZomCat[RandCat], spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        BallScript t1 = newZomCat.GetComponent<BallScript>();        
        Animator newZomCatAnim = newZomCat.GetComponent<Animator>();
        t1.speed = rand;
        newZomCatAnim.speed = t1.speed;
        t1.gameControl = this;
        newZomCat.GetComponent<Kill>().gameControl = this;
        ZomCatList.Add(newZomCat);
        CountadvNow++;
        CatSpawedFromStart++;
        

    }
    
    void FixedUpdate()
    {
        //пауза игры
        if (!isStopGame)
        {

            foreach (GameObject item in ZomCatList)
            {
                Rigidbody2D rb = item.GetComponent<Rigidbody2D>();
                BallScript t1 = item.GetComponent<BallScript>();
                rb.MovePosition(rb.position + Vector2.left * speed * t1.speed * Time.deltaTime);
            }


            if (_IsSpawNow && LastTimeSpaw + frequncySpaw < Time.time && CatSpawedFromStart < MaxCatSpaw && ZomCatList.Count < SpawCat)
            {
                
                Spawn();
                LastTimeSpaw = Time.time;              

            }


            if (CatSpawedFromStart >= MaxCatSpaw && ZomCatList.Count == 0)
            {                
                StartCoroutine(StopGame());
                CurrentLevel++;
                _IsSpawNow = false;                
            }

        }

        ///--------------------------------------------
        /////for Player
        //стрельба

        if (!Player.IsShootingNow)
        {
            if (Input.GetButtonDown("Fire2"))  //Если игрок нажал на e
            {

                //Вызов метода стрельбы снарядами            
                _player.ShootBulletFly();                
            }

            if (ShotBullet)
            {
                _player.ShootBulletFly();                
            }



            if (Input.GetButtonDown("Fire1"))  //Если игрок нажал на q
            {

                //Вызов метода стрельбы снарядами
                StartCoroutine(_player.GranyAttack());
                _player.ShootBullet();
                   
            }

            if (UpShotBullet)
            {
                _player.ShootBullet();             
            }

        }

        //ходьба

        float yP = Camera.main.WorldToScreenPoint(transform.position).y;
        rb = GetComponent<Rigidbody2D>();
        float moveY = Input.GetAxis("Vertical");
        bool isMoveUp = true;      
    }
    
    public void OnApplicationQuit()
    {
        SaveRekord();
    }

    public void score1()
    {
        Score++;
        ScoreText.text = Score+"";
        HeighScore();        
    }

    public void HeighScore()
    {
        
        if (Score > HighScoreCounter)
        {
            HighScoreCounter = Score;            
            //HighScore.text = HighScoreCounter + "";            
        }
    }

    public void DestroyZomCat(GameObject ZomCat)
    {
        for (int i = ZomCatList.Count - 1; i >= 0; i--)
        {
            if (ZomCatList[i].Equals(ZomCat))
           {                
               ZomCatList.RemoveAt(i);                
                ZomCat.GetComponent<BoxCollider2D>().enabled = false;                
                score1();

            }
        }
}

    public void Coin()
    {
        CoinCounter+=100;
        CoinText.text = CoinCounter + "";

    }

    IEnumerator StopGame()
    {
        CatSpawedFromStart = 0;
        //Увеличение волн        
        WavesSkore++;
        WavesText.text = WavesSkore + "";            
        //Увеличение монеток
        //CoinCounter += 10;
        //CoinText.text = CoinCounter + "";

        if (WavesSkore > 4)
        {
            SpedCats += 0.01f;
            //увеличене уровня
            Level++;                
            LevelPlus.text = Level + "";         
            //Kill.NewHealthCats++;                
            WavesSkore = 0;
            WavesText.text = WavesSkore + "";
            Coin();
        }

        if (Level > Nextlevel)
        {
            Nextlevel+=2;
            Kill.NewHealthCats++;
            MaxCatSpaw++;
        }

        StartCoroutine(TimeLevel());
        yield return new WaitForSeconds(5f);        
        //MaxCatSpaw++;
        _IsSpawNow = true;     
    }

    //счетчик времени
    IEnumerator TimeLevel()
    {       
        for(int i = 0; i < 5; i++) 
        {            
            TimeSkore--;
            TimeText.text = TimeSkore + "";            
            yield return new WaitForSeconds(1f);
        };
        TimeSkore=5;
        TimeText.text = TimeSkore + "";
            
    }
    
    public void SaveRekord()
    {
        //Debug.Log(WavesSkore + " =Волны" + "//" + MaxCatSpaw + " =Количество котов" + "//" + Level + " =Уровень" + "//" + Kill.NewHealthCats + "=жизни кота" + "//" + Score + "=Запись счета" + "//" + HighScoreCounter + "=Запись рекорда");
        PlayerPrefs.SetInt("SaveWavesSkore", WavesSkore);//Запись волн
        PlayerPrefs.SetInt("SaveMaxSpawCats", MaxCatSpaw);//Количество котов
        //Debug.Log(Level + "Уровня");
        PlayerPrefs.SetInt("SaveLevel", Level);//Запись Уровня
        PlayerPrefs.SetInt("SaveNextLevel", Nextlevel);//Запись Уровня
        //Debug.Log(Kill.NewHealthCats + "жизни кота");
        PlayerPrefs.SetInt("SaveHealthCats", Kill.NewHealthCats);//Запись жизни кота
        //Debug.Log(Score + "Запись счета");
        PlayerPrefs.SetInt("SaveScoreLevel", Score);//Запись счета
        //Debug.Log(HighScoreCounter + "Запись рекорда");
        PlayerPrefs.SetInt("SaveScore", HighScoreCounter);//Запись рекорда

    }

    public void DeleteRekord()
    {
        //Debug.Log(WavesSkore + " =Волны" + "//" + MaxCatSpaw + " =Количество котов" + "//" + Level + " =Уровень" + "//" + Kill.NewHealthCats + "=жизни кота" + "//" + Score + "=Запись счета" + "//" + HighScoreCounter + "=Запись рекорда");
        PlayerPrefs.DeleteKey("SaveWavesSkore");//запись волны
        PlayerPrefs.DeleteKey("SaveLevel");//Запись Уровня
        PlayerPrefs.DeleteKey("SaveScoreLevel");//Запись счета
        PlayerPrefs.DeleteKey("SaveHealthCats");//Запись жизни кота
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void Remove()
    {
        
        isShooting = false;
        isStopGame = false;
        MaxCatSpaw = 1;
        CatSpawedFromStart = 0;
        Kill.healthCats = 1;
        Kill.NewHealthCats = 1;
        Player.DamageForEnemy = 1;
        Player.GameIsPaused = false;
        GameIsPaused = false;
        //Debug.Log("RestartExit");        
        Time.timeScale = 1f;

        isSave = false;

        Level = 1;
        LevelPlus.text = Level + "";

        WavesSkore = 1;
        WavesText.text = WavesSkore + "";


        DeleteRekord();
    }

    //--------------------------------------------кнопки для меню --------------------------------------------//
    #region Button


    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

   

    //кнопка продолжить
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Debug.Log("Resume");
    }

    //кнопка паузы
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Debug.Log("Pause");
    }

    //кнопка выхода в главное меню
    public void MainMenu()
    {
        Debug.Log("menu");
    }

    //кнопка выхода 
    public void Restart()
    {
                
        isShooting = false;
        isStopGame = false;
        MaxCatSpaw = 1;
        CatSpawedFromStart = 0;
        Kill.healthCats = 1;
        Kill.NewHealthCats = 1;
        Player.DamageForEnemy = 1;
        Player.GameIsPaused = false;        
        GameIsPaused = false;                
        Time.timeScale = 1f;

        isSave = false;
        
        Level = 1;
        LevelPlus.text = Level + "";

        WavesSkore = 1;
        WavesText.text = WavesSkore + "";

        DeleteRekord();
                
    }

    //переход на другую сцену
    public void LoadScene(int Level)
    {
        Remove();
        SceneManager.LoadScene(Level);
       

       // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }

    public void LoadSceneMenu(int Level)
    {
        SaveRekord();
        SceneManager.LoadScene(Level);
       // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Exit()
    {
        Application.Quit();
    }

    //игра с начала 
    public void Retry()
    {


    }
    #endregion Button
}

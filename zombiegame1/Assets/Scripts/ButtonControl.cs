﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonControl : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public GameControl gameControl;
    public Player _player;

    private ButtonControl player;

    public EnumButton enumButton;
    
	// Use this for initialization
	void Start ()
    {
		player = FindObjectOfType<ButtonControl> ();
	}


	public static void UpArrow()
    {
        Player.moveUp = true;
        Player.moveDown = false;		
		//Debug.Log ("MoveUp");
	}


	public static void DownArrow()
    {
		Player.moveDown = true;
		Player.moveUp = false;

		//Debug.Log ("MoveDown");
	}


	public void ReleaseUpArrow()
    {
		Player.moveDown = false;
	}

	public void ReleaseDownArrow()
    {
		Player.moveUp = false;
	}

    public void Shooting()
    {
        //Player.ShotBullet = true;
        StartCoroutine(TimeShot());       
    }
    IEnumerator TimeShot()
    {
        yield return new WaitForSeconds(0.001f);
       // Player.ShotBullet = false;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (enumButton)
        {
            case EnumButton.ArrowUp:
                Player.moveUp = true;
                //Debug.Log("TrueUp");
                break;
            case EnumButton.ArrowDown:
                Player.moveDown = true;
                //Debug.Log("TrueDown");
                break;
            case EnumButton.Shot:
                GameControl.ShotBullet = true;
                //Debug.Log("shottrue");
                break;

            case EnumButton.UpShot:
                GameControl.UpShotBullet = true;
                //Debug.Log("shottrue");
                break;

            default:
                break;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (enumButton)
        {
            case EnumButton.ArrowUp:
                Player.moveUp = false;
               // Debug.Log("falseUp");
                break;
            case EnumButton.ArrowDown:
                Player.moveDown = false;
                //Debug.Log("falseDown");
                break;
            case EnumButton.Shot:
                GameControl.ShotBullet = false;
                //Debug.Log("shotfalse");
                break;
            case EnumButton.UpShot:
                GameControl.UpShotBullet = false;
                //Debug.Log("shotfalse");
                break;
            default:
                break;
        }
    }
}

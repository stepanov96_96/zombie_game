﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDust : MonoBehaviour
{
    public GameObject Dust;


    public void Start()
    {
        StartCoroutine (DestroyEfects());
    }

    IEnumerator DestroyEfects()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(Dust);
    }
}

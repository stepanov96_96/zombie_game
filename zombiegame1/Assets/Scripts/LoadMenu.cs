﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadMenu : MonoBehaviour
{
    public Text HighScore;
    public static LoadMenu instance;
    public GameObject PanelUI;
    public GameObject PanelSoundOFF;
    public GameObject PanelSoundON;
    public static bool Sound = false;
    private int SavePanelOFF = 0;
    private int SavePanelON = 0;

    private void Awake()
    {
        instance = this;
        if (PlayerPrefs.HasKey("SaveScore"))
        {
            GameControl.HighScoreCounter = PlayerPrefs.GetInt("SaveScore");
            HighScore.text = GameControl.HighScoreCounter + "";
        }

        if (PlayerPrefs.HasKey("SavePanelON"))
        {
            SavePanelON = PlayerPrefs.GetInt("SavePanelON");
           // Debug.Log("SavePanelON"+ SavePanelON);

            if (SavePanelON==1)
            {
                PanelSoundOFF.SetActive(false);
                PanelSoundON.SetActive(true);
            }
            else
            {
                PanelSoundON.SetActive(false);
                PanelSoundOFF.SetActive(true);
            }

        }

        if (PlayerPrefs.HasKey("SavePanelOFF"))
        {
            SavePanelOFF = PlayerPrefs.GetInt("SavePanelOFF");
           // Debug.Log("SavePanelOFF=" + SavePanelOFF);

            if (SavePanelOFF == 1)
            {
                PanelSoundOFF.SetActive(true);
                PanelSoundON.SetActive(false);
            }
            else
            {
                PanelSoundON.SetActive(true);
                PanelSoundOFF.SetActive(false);
            }

        }


    }  

    //переход на другую сцену
    public void LoadScene(int Level)
    {
        GameControl.isSave = true;
        SceneManager.LoadScene(Level);
        //Debug.Log(GameControl.isSave);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Retry(int Level)
    {
        SceneManager.LoadScene(Level);
    }

    public void Return()
    {
        PanelUI.SetActive(false);
    }

    public void PrivacyPolicy()
    {
        PanelUI.SetActive(true);        
    }

    public void SondOFF()
    {
        PlayerPrefs.DeleteKey("SavePanelOFF");
        PlayerPrefs.DeleteKey("SavePanelON");
        PanelSoundOFF.SetActive(false);
        PanelSoundON.SetActive(true);

        if (PanelSoundON == true)
        {
            SavePanelON = 1;                       
        }
        
        SavePanelOFF = 0;
        Sound = true;
        PlayerPrefs.SetInt("SavePanelON", SavePanelON);
    }

    public void SondON()
    {
        PlayerPrefs.DeleteKey("SavePanelOFF");
        PlayerPrefs.DeleteKey("SavePanelON");
        PanelSoundON.SetActive(false);
        PanelSoundOFF.SetActive(true);
          

        if (PanelSoundOFF == true)
        {
            SavePanelOFF = 1;
            //Debug.Log("SavePanelOFF="+ SavePanelOFF);
        }
        
        SavePanelON = 0;
        Sound = false;
        PlayerPrefs.SetInt("SavePanelOFF", SavePanelOFF);
    }
}
